FROM debian:testing
MAINTAINER hans@eds.org

ENV LANG=C.UTF-8 \
    DEBIAN_FRONTEND=noninteractive

# install the minimum needed for a standard git-buildpackage build
# that includes pristine-tar.
#
# * update-alternatives needs ../man1/
# * ca-certificates for fetching from HTTPS repos
RUN mkdir -p /usr/share/man/man1 \
	&& apt-get update \
	&& apt-get -qy upgrade \
	&& apt-get -qy dist-upgrade \
	&& export build_deps=' \
		build-essential \
		ca-certificates \
		fakeroot \
		git-buildpackage \
		lintian \
		pristine-tar' \
	&& apt-get -qy install --no-install-recommends $build_deps \
		autodep8 \
		autopkgtest \
		git \
	&& apt-get -qy autoremove --purge \
	&& apt-get clean \
	&& apt-mark auto $build_deps \
	&& rm -rf /var/lib/apt/lists/*

COPY gitlab-ci-common /usr/share/
COPY gitlab-ci-add-pages-repo /usr/bin/
COPY gitlab-ci-aptly /usr/bin/
COPY gitlab-ci-autopkgtest /usr/bin/
COPY gitlab-ci-entrypoint /usr/bin/
COPY gitlab-ci-git-buildpackage /usr/bin/
COPY gitlab-ci-git-buildpackage-all /usr/bin/
COPY gitlab-ci-lintian /usr/bin/
COPY gitlab-ci-enable-sid /usr/bin/
COPY gitlab-ci-enable-experimental /usr/bin/

ENTRYPOINT ["/usr/bin/gitlab-ci-entrypoint"]
