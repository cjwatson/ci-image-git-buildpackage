
This is a Docker image for running GitLab CI builds for packages based
on git-buildpackage.  A lot of the assumptions of how GitLab CI works
conflict with git-buildpackage, so this image should help things.

It should also provide a local cache of the base packages needed to
run git-buildpackage builds.
